class Point
{
  float x;
  float y;
 
  Point(float _x, float _y)
  {
    x = _x;
    y = _y;
  }
}
 
ArrayList<Point> listEllipsePoint;
ArrayList<Point> curvePoints = new ArrayList<Point>();
ArrayList<Point> cyclePoints = new ArrayList<Point>();

//Variables
float x = 300;
float y = 200;
float a = 250;
float b = 150;
Point A = new Point(0,100);
Point Cycle = new Point(400,400);


float c = sqrt(pow(a,2)-pow(b,2));
Point Focus = new Point(x+c,y);
float R = getDistance(Focus,Cycle);
int stepi = 1;
int i = 1;
int cykle_ind = 0;
int cykle_step = 1;
float eps = 0.005;
int stepx = 3;

void setup() {
    size(1200, 1000);
    background(170, 170, 255);
    findCyclePoints();

}
 
void draw() {
try{
  background(153, 153, 255);
  //find ellipse points
  findPoint(x, y);
  //draw ellipce
  //ellipse(x,y,a*2,b*2);
  drawEllipce();
  if (mousePressed == true) {
    curvePoints.add(new Point(mouseX, mouseY));
  }
  if(curvePoints.size() >0){
    if(i >= curvePoints.size()){
    stepi = -stepi;
    i-=1;
   }
  point(A.x, A.y);
  A.x = curvePoints.get(i).x;
  A.y = curvePoints.get(i).y;
  drawTangents();
  if(i < 1){
    stepi = -stepi;
    }
  i+=stepi;
  
  
  x = cyclePoints.get(cykle_ind).x -c;
  y = cyclePoints.get(cykle_ind).y; 
  
  cykle_ind++;
  if(cykle_ind >= cyclePoints.size())
  {
    cykle_ind = 0;
  }
  findCyclePoints();
 }
}
catch(Exception ex){
    i = 0;
    curvePoints = new ArrayList<Point>();
  }
}

boolean isTangent(float _x, float _y)
{
  if(((_x - x) * (A.x - x))/(a*a) + ((_y -y) * (A.y - y))/(b*b) >= 1 -eps
  && ((_x - x) * (A.x - x))/(a*a) + ((_y -y) * (A.y - y))/(b*b) <= 1 + eps)
  {
    return true;
  }
 
 
  return false;
}
void drawTangents(){
  for(int i=0; i<listEllipsePoint.size(); i++)
  {
     if(isTangent(listEllipsePoint.get(i).x,listEllipsePoint.get(i).y)){
       line(listEllipsePoint.get(i).x,listEllipsePoint.get(i).y, A.x,A.y);
       float x2 = listEllipsePoint.get(i).x;
       float y2 = listEllipsePoint.get(i).y;
       
       if(A.y > listEllipsePoint.get(i).y){
         line(A.x,A.y,((-A.y)*(x2-A.x))/(y2-A.y) + A.x,0);
       }
       else if(A.y < listEllipsePoint.get(i).y){
                 line(A.x,A.y,((1000-A.y)*(x2-A.x))/(y2-A.y) + A.x,1000);
       }
      }
       
       
    }
  }

void findCyclePoints(){
  for (float i=0; i<360;i+=1) {
    float x=Cycle.x+R*cos(radians(i));
    float y=Cycle.y+R*sin(radians(i));
    cyclePoints.add(new Point(x,y));
    point(x,y);
  }
}

 void findPoint(float x_center, float y_center)
{
  listEllipsePoint = new ArrayList<Point>();
    for (float i=0; i<360;i+=0.1) {
    float x=x_center+a*cos(radians(i));
    float y=y_center+b*sin(radians(i));
    listEllipsePoint.add(new Point(x,y));
    point(x,y);
  }
}

void drawEllipce(){
  for(int i = 1;i<listEllipsePoint.size();i++){
    line(listEllipsePoint.get(i-1).x,listEllipsePoint.get(i-1).y,listEllipsePoint.get(i).x,listEllipsePoint.get(i).y);
  }
}

float getDistance(Point p1, Point p2){
  return sqrt(pow(p1.x-p2.x,2) + pow(p1.y-p2.y,2));
}

boolean isInEllipse(float _x, float _y, float x_center, float y_center)
{
  if(((pow(_x-x_center,2))/(pow(a,2)))+((pow(_y-y_center,2))/(pow(b,2)))>= 1 -eps
  && ((pow(_x-x_center,2))/(pow(a,2)))+((pow(_y-y_center,2))/(pow(b,2))) <= 1+eps)  
  {  
    return true;
  }    
  return false;
}
